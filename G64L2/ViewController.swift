//
//  ViewController.swift
//  G64L2
//
//  Created by Timur Astashov on 7/9/18.
//  Copyright © 2018 Timur Astashov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      
    }
    
    func typePrivet() {
        print("Привет")
    }
    
    func findDayOfTheWeek(number: Int) {
        if number == 1{
            print("Monday")
        }
        if number == 2{
            print("Tuesday")
        }
        if number == 3{
            print("Wednesday")
        }
        if number == 4{
            print("Thursday")
        }
        if number == 5{
            print("Friday")
        }
        if number == 6{
            print("Saturday")
        }
        if number == 7{
            print("Sunday")
        }
    }
    
    func summOfTwoNumbers(number1: Int, number2: Int){
        let summ = number1 + number2
        print(summ)
    }
    
    func Ex4(number4: Int) -> Int{
        let finalNumber=number4*30+5
        return finalNumber
    }
    
    func typePrivetFewTimes(quantityPovtoreniiSlovaPrivet: Int){
        for _ in 0..<quantityPovtoreniiSlovaPrivet{
            print("Privet")
    }
    }
    
    func fibonacci(fib: Int) -> Int{
        var fibPrevious = 0
        var fibCurrent = 1
        while fibCurrent < fib{
            let fibSumm = fibPrevious + fibCurrent
            fibPrevious = fibCurrent
            fibCurrent  = fibSumm
    }
        return fibCurrent
    }
    
    
}







